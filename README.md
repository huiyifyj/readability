# Readability

![GoDoc](https://img.shields.io/badge/godoc-pkg.go.dev-blue?style=flat-square&logo=go)
![LICENSE](https://img.shields.io/badge/license-MIT-brightgreen?style=flat-square)

📖 `Readability` 是 go 语言的库, 基于 DOM 结构特征, 从 HTML 页面中找到主要的可读文字内容和一些元数据(如作者、标题、摘要等)。并消去和移除大部分网页中混乱的按钮, 广告, 背景图片, 以及一些 JS 脚本...

## 安装方法

安装该库只需运行命令行 `go get`:
```shell
go get -u -v gitea.com/huiyifyj/readability
```

## 使用案例

使用 `readability.FromURL()` 方法就可以通过 URL 参数去获取文字内容。它将会从指定的 URL 中获取网页, 检查其是否可读, 然后返回已经找到的文字内容:
```go
package main

import (
	"fmt"
	"log"
	"os"
	"time"

	readability "gitea.com/huiyifyj/readability"
)

var (
	urls = []string{
		// 这是一篇可解析的文字
		"https://huiyifyj.cn/a-case-for-using-void-in-modern-javascript/",
		// 这并不是一篇文字, 所以它可能会解析失败
		"https://huiyifyj.cn",
	}
)

func main() {
	for i, url := range urls {
		article, err := readability.FromURL(url, 30*time.Second)
		if err != nil {
			log.Fatalf("Failed to parse %s, %v\n", url, err)
		}

		dstTxtFile, _ := os.Create(fmt.Sprintf("text-%02d.txt", i+1))
		defer dstTxtFile.Close()
		dstTxtFile.WriteString(article.TextContent)

		dstHTMLFile, _ := os.Create(fmt.Sprintf("html-%02d.html", i+1))
		defer dstHTMLFile.Close()
		dstHTMLFile.WriteString(article.Content)

		fmt.Printf("URL     : %s\n", url)
		fmt.Printf("Title   : %s\n", article.Title)    // 文章标题
		fmt.Printf("Author  : %s\n", article.Byline)   // 文章作者
		fmt.Printf("Length  : %d\n", article.Length)   // 文章长度
		fmt.Printf("Excerpt : %s\n", article.Excerpt)  // 文章摘要
		fmt.Printf("SiteName: %s\n", article.SiteName) // 站点名称
		fmt.Printf("Image   : %s\n", article.Image)    // 文章图片
		fmt.Printf("Favicon : %s\n", article.Favicon)  // 文章图标
		fmt.Printf("Text content saved to \"text-%02d.txt\"\n", i+1)
		fmt.Printf("HTML content saved to \"html-%02d.html\"\n", i+1)
		fmt.Println()
	}
}
```

但是, 有时候无论该 URL 的内容是不是文章, 你都想去解析。比如, 你只是想去获取该页面的元数据(如, 网页的标题、摘要等等)。
那么, 你就需要使用 `http.Get` 将 URL 的页面下载下来, 然后使用 `readability.FromReader()` 对其进行解析:
```go
package main

import (
	"fmt"
	"log"
	"net/http"

	readability "gitea.com/huiyifyj/readability"
)

var (
	urls = []string{
		// 文字内容和元数据都能正常解析出来
		"https://huiyifyj.cn/a-case-for-using-void-in-modern-javascript",
		// 但是这个不会解析出文字内容, 但是可以解析出元数据
		"https://huiyifyj.cn",
	}
)

func main() {
	for _, url := range urls {
		resp, err := http.Get(url)
		if err != nil {
			log.Fatalf("Failed to download %s: %v\n", url, err)
		}
		defer resp.Body.Close()

		article, err := readability.FromReader(resp.Body, url)
		if err != nil {
			log.Fatalf("Failed to parse %s: %v\n", url, err)
		}

		fmt.Printf("URL     : %s\n", url)
		fmt.Printf("Title   : %s\n", article.Title)
		fmt.Printf("Author  : %s\n", article.Byline)
		fmt.Printf("Length  : %d\n", article.Length)
		fmt.Printf("Excerpt : %s\n", article.Excerpt)
		fmt.Printf("SiteName: %s\n", article.SiteName)
		fmt.Printf("Image   : %s\n", article.Image)
		fmt.Printf("Favicon : %s\n", article.Favicon)
		fmt.Println()
	}
}
```

## 命令行使用

你也可以使用 `go-readability` 作为命令行使用。只需要通过以下命令安装:
```shell
go get -u -v gitea.com/huiyifyj/readability/cmd/...
```

安装后你就可以在终端界面使用 `go-readability`:
```shell
$ go-readability -h

go-readability is parser to fetch the readable content of a web page.
The source can be an url or existing file in your storage.

Usage:
  go-readability [flags] source

Flags:
  -h, --help       help for go-readability
  -m, --metadata   only print the page's metadata
```

## 协议

> `Readability` 库遵守 [MIT](https://opensource.org/licenses/MIT) 协议

这意味你可以随意改动, 只要注明我 [@huiyifyj](https://github.com/huiyifyj) 即可。
